/*
 * $RCSfile: SqlDataExport.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-3-12 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.exchange;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.skin.webcat.database.Column;
import com.skin.webcat.database.Table;
import com.skin.webcat.database.handler.TableHandler;
import com.skin.webcat.exchange.transform.DataTransform;
import com.skin.webcat.exchange.transform.DateTransform;
import com.skin.webcat.exchange.transform.NumberTransform;
import com.skin.webcat.exchange.transform.StringTransform;
import com.skin.webcat.util.StringUtil;

/**
 * <p>Title: SqlDataExport</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class SqlDataExport extends DataExport {
    /**
     *
     */
    public SqlDataExport() {
        super();
    }

    /**
     * @param connection
     */
    public SqlDataExport(Connection connection) {
        super(connection);
    }

    /**
     * @param tableName
     * @param out
     */
    @Override
    public void execute(String tableName, PrintWriter out) throws Exception {
        String name = StringUtil.replace(tableName, "'", "");
        int k = name.indexOf(" ");

        if(k > -1) {
            name = name.substring(0, k);
        }

        PreparedStatement statement = null;
        ResultSet resultSet = null;

        StringBuilder sql = new StringBuilder("select * from ");
        sql.append(name);

        statement = this.connection.prepareStatement(sql.toString());
        resultSet = statement.executeQuery();
        ResultSetMetaData metaData = resultSet.getMetaData();

        TableHandler tableHandler = new TableHandler(this.connection);
        Table table = tableHandler.getTable(metaData);
        List<Column> columns = table.getColumns();
        List<DataTransform> transforms = new ArrayList<DataTransform>(columns.size());

        for(Column column : columns) {
            String javaTypeName = column.getJavaTypeName();

            if(javaTypeName.equalsIgnoreCase("boolean") || javaTypeName.equalsIgnoreCase("Boolean")
                    || javaTypeName.equalsIgnoreCase("byte") || javaTypeName.equalsIgnoreCase("Byte")
                    || javaTypeName.equalsIgnoreCase("short") || javaTypeName.equalsIgnoreCase("Short")
                    || javaTypeName.equalsIgnoreCase("int") || javaTypeName.equalsIgnoreCase("Integer")
                    || javaTypeName.equalsIgnoreCase("float") || javaTypeName.equalsIgnoreCase("Float")
                    || javaTypeName.equalsIgnoreCase("double") || javaTypeName.equalsIgnoreCase("Double")
                    || javaTypeName.equalsIgnoreCase("long") || javaTypeName.equalsIgnoreCase("Long")
            ) {
                transforms.add(new NumberTransform());
            }
            else if(javaTypeName.equalsIgnoreCase("String")) {
                transforms.add(new StringTransform("'"));
            }
            else if(javaTypeName.equalsIgnoreCase("java.util.Date")) {
                DateTransform dateTransform = new DateTransform(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
                dateTransform.setQuote("'");
                transforms.add(dateTransform);
            }
            else {
                transforms.add(new StringTransform());
            }
        }

        int i = 0;
        int size = columns.size();
        StringBuilder buffer = new StringBuilder();
        buffer.append("insert into ");
        buffer.append(tableName);
        buffer.append("(");
        
        for(i = 0; i < size; i++) {
            Column column = columns.get(i);
            buffer.append(column.getColumnName());

            if(i < (size - 1)) {
                buffer.append(", ");
            }
        }

        buffer.append(") values (");
        size = size - 1;
        String insert = buffer.toString();

        while(resultSet.next()) {
            out.print(insert);

            for(i = 0; i < size; i++) {
                Column column = columns.get(i);
                DataTransform transform = transforms.get(i);
                out.print(transform.toString(this.getObject(column, resultSet)));
                out.print(", ");
            }

            if(size > -1) {
                Column column = columns.get(i);
                DataTransform transform = transforms.get(i);
                out.print(transform.toString(this.getObject(column, resultSet)));
                out.println(");");
            }
        }
        out.flush();
    }
}
