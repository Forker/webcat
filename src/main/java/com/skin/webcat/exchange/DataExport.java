/*
 * $RCSfile: DataExport.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-3-12 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.exchange;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.skin.webcat.database.Column;
import com.skin.webcat.util.IO;

/**
 * <p>Title: DataExport</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public abstract class DataExport {
    private boolean header;
    protected Connection connection;

    /**
     * 
     */
    public DataExport() {
        this.header = true;
    }

    /**
     * @param connection
     */
    public DataExport(Connection connection) {
        this.header = true;
        this.connection = connection;
    }

    /**
     * @param tableName
     * @param file
     * @throws Exception
     */
    public void execute(String tableName, File file) throws Exception {
        OutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(file);
            this.execute(tableName, outputStream, "UTF-8");
        }
        finally {
            IO.close(outputStream);
        }
    }

    /**
     * @param tableName
     * @param outputStream
     * @param charset
     * @throws Exception
     */
    public void execute(String tableName, OutputStream outputStream, String charset) throws Exception {
        PrintWriter out = null;
        OutputStreamWriter writer = null;

        try {
            writer = new OutputStreamWriter(outputStream, charset);
            out = new PrintWriter(writer);
            this.execute(tableName, out);
            out.flush();
        }
        finally {
        }
    }

    /**
     * @param tableName
     * @param out
     * @throws Exception
     */
    public abstract void execute(String tableName, PrintWriter out) throws Exception;

    /**
     * @param column
     * @param resultSet
     * @return Object
     * @throws SQLException 
     */
    public Object getObject(Column column, ResultSet resultSet) throws SQLException {
        String typeName = column.getJavaTypeName();
        String columnName = column.getColumnName();

        if(typeName == null) {
            return resultSet.getObject(columnName);
        }
        else if(typeName.equals("int") || typeName.equals("java.lang.Integer")) {
            return resultSet.getInt(columnName);
        }
        else if(typeName.equals("float") || typeName.equals("java.lang.Float")) {
            return resultSet.getFloat(columnName);
        }
        else if(typeName.equals("double") || typeName.equals("java.lang.Double")) {
            return resultSet.getDouble(columnName);
        }
        else if(typeName.equals("long") || typeName.equals("java.lang.Long")) {
            return resultSet.getLong(columnName);
        }
        else if(typeName.equals("String")) {
            return resultSet.getString(columnName);
        }
        else if(typeName.equals("Date") || typeName.equals("java.util.Date") || typeName.equals("java.sql.Date")) {
            return resultSet.getTimestamp(columnName);
        }
        else {
            return resultSet.getObject(columnName);
        }
    }
    
    /**
     * @return the header
     */
    public boolean getHeader() {
        return this.header;
    }

    /**
     * @param header the header to set
     */
    public void setHeader(boolean header) {
        this.header = header;
    }

    /**
     * @return the connection
     */
    public Connection getConnection() {
        return this.connection;
    }

    /**
     * @param connection the connection to set
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
