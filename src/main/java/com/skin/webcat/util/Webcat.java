/*
 * $RCSfile: Webcat.java,v $$
 * $Revision: 1.1 $
 * $Date: 2014-03-19 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.skin.resource.PropertyResource;
import com.skin.util.IO;
import com.skin.webcat.datasource.ConnectionManager;

/**
 * <p>Title: Webcat</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class Webcat {
    private static Logger logger = LoggerFactory.getLogger(Webcat.class);

    /**
     * @return List<String>
     */
    public static List<String> getConnectionList() {
        return ConnectionManager.getConfigNameList();
    }

    /**
     * @param name
     * @return Connection
     * @throws SQLException
     */
    public static Connection getConnection(String name) throws SQLException {
        return ConnectionManager.getConnection(name);
    }

    /**
     * @param name
     * @param database
     * @return Connection
     * @throws SQLException
     */
    public static Connection getConnection(String name, String database) throws SQLException {
        return ConnectionManager.getConnection(name, database);
    }

    /**
     * @param resource
     * @return Properties
     */
    public static Properties getProperties(String resource) {
        try {
            return getProperties(Webcat.class.getClassLoader().getResourceAsStream(resource));
        }
        catch(Exception e) {
            logger.error(e.getMessage(), e);
        }
        return new Properties();
    }

    /**
     * @param inputStream
     * @return Properties
     * @throws Exception
     */
    public static Properties getProperties(InputStream inputStream) throws Exception {
        if(inputStream != null) {
            Map<String, String> config = PropertyResource.load(inputStream, "UTF-8");
            Properties properties = new Properties();

            for(Map.Entry<String, String> entry : config.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                properties.setProperty(key, value);
            }
            return properties;
        }
        return null;
    }

    /**
     * @param name
     * @return String
     */
    public static String camel(String name) {
        if(null == name || name.trim().length() < 1) {
            return "";
        }

        String[] subs = name.split("_");
        StringBuilder buffer = new StringBuilder();

        if(name.startsWith("_")) {
            buffer.append("_");
        }

        if(subs.length == 1) {
            String s = subs[0];

            if("ID".equals(s)) {
                buffer.append("Id");
            }
            else if(s.toUpperCase().equals(s)) {
                buffer.append(Character.toUpperCase(s.charAt(0)));
                buffer.append(s.substring(1).toLowerCase());
            }
            else {
                buffer.append(Character.toUpperCase(s.charAt(0))).append(s.substring(1));
            }
        }
        else {
            for(String s : subs) {
                if(s.length() > 0) {
                    if("ID".equals(s)) {
                        buffer.append(s);
                    }
                    else if(s.toUpperCase().equals(s)) {
                        buffer.append(Character.toUpperCase(s.charAt(0)));
                        buffer.append(s.substring(1).toLowerCase());
                    }
                    else {
                        buffer.append(Character.toUpperCase(s.charAt(0))).append(s.substring(1));
                    }
                }
            }
        }

        if(name.endsWith("_")) {
            buffer.append("_");
        }
        return buffer.toString();
    }

    /**
     * @param filePath
     * @return String
     */
    public static String getSource(String filePath) {
        try {
            if(filePath.endsWith(".link.sql")) {
                String content = IO.read(new File(filePath), "UTF-8");
                String[] list = content.split("\\n");
                StringBuilder buffer = new StringBuilder();
    
                for(String line : list) {
                    line = line.trim();
    
                    if(line.startsWith("#")) {
                        continue;
                    }

                    if(line.endsWith(".sql")) {
                        String sql = IO.read(new File(line), "UTF-8");
                        buffer.append(sql);
                    }
                }
                return buffer.toString();
            }
            else if(filePath.endsWith(".sql")) {
                return IO.read(new File(filePath), "UTF-8");
            }
            else {
                return "";
            }
        }
        catch(IOException e) {
            logger.warn(e.getMessage(), e);
        }
        return "";
    }
}
