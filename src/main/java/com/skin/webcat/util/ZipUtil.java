/*
 * $RCSfile: ZipUtil.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-4-1 $
 *
 * Copyright (C) 2008 WanMei, Inc. All rights reserved.
 *
 * This software is the proprietary information of WanMei, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * <p>Title: ZipUtil</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author chenyankui
 * @version 1.0
 */
public class ZipUtil {
    /**
     * @param dir
     * @param target
     */
    public static void compress(String dir, String target) {
        OutputStream outputStream = null;

        try {
            outputStream = new FileOutputStream(target);
            compress(dir, outputStream);
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            if(outputStream != null) {
                try {
                    outputStream.close();
                }
                catch(IOException e) {
                }
            }
        }
    }

    /**
     * @param dir
     * @param outputStream
     */
    public static void compress(String dir, OutputStream outputStream) {
        File file = new File(dir);
        ZipOutputStream zipOutputStream = null;

        try {
            zipOutputStream = new ZipOutputStream(outputStream);
            compress(file, zipOutputStream, "");
            zipOutputStream.finish();
            zipOutputStream.flush();
            outputStream.flush();
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            if(zipOutputStream != null) {
                try {
                    zipOutputStream.close();
                }
                catch(IOException e) {
                }
            }
        }
    }

    /**
     * @param file
     * @param zipOutputStream
     * @param basedir
     * @throws IOException
     */
    public static void compress(File file, ZipOutputStream zipOutputStream, String basedir) throws IOException {
        if(file.isDirectory()) {
            compressDirectory(file, zipOutputStream, basedir);
        }
        else {
            compressFile(file, zipOutputStream, basedir);
        }
    }

    /**
     * @param dir
     * @param zipOutputStream
     * @param basedir
     * @throws IOException
     */
    public static void compressDirectory(File dir, ZipOutputStream zipOutputStream, String basedir) throws IOException {
        File[] files = dir.listFiles();

        if(files.length < 1) {
            ZipEntry entry = new ZipEntry(basedir + dir.getName() + "/");
            zipOutputStream.putNextEntry(entry);
            zipOutputStream.closeEntry();
        }
        else {
            for(int i = 0; i < files.length; i++) {
                compress(files[i], zipOutputStream, basedir + dir.getName() + "/");
            }
        }
    }

    /**
     * @param file
     * @param zipOutputStream
     * @param basedir
     */
    public static void compressFile(File file, ZipOutputStream zipOutputStream, String basedir) {
        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream(file);
            ZipEntry entry = new ZipEntry(basedir + file.getName());
            zipOutputStream.putNextEntry(entry);
            copy(inputStream, zipOutputStream, 4096);
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
        finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                }
                catch(IOException e) {
                }
            }
        }
    }

    /**
     * @param inputStream
     * @param outputStream
     * @param bufferSize
     * @throws IOException
     */
    private static void copy(InputStream inputStream, OutputStream outputStream, int bufferSize) throws IOException {
        int length = 0;
        byte[] bytes = new byte[Math.max(bufferSize, 4096)];

        while((length = inputStream.read(bytes)) > -1) {
            outputStream.write(bytes, 0, length);
        }

        outputStream.flush();
    }
}
