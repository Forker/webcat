/*
 * $RCSfile: RunResult.java,v $$
 * $Revision: 1.1 $
 * $Date: 2016-3-19 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package com.skin.webcat.database.sql;

import java.util.List;

import com.skin.webcat.database.Column;

/**
 * <p>Title: RunResult</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class SqlResult {
    private int status;
    private String message;
    private List<Column> columns;
    private List<Record> records;

    /**
     * 
     */
    public SqlResult() {
    }

    /**
     * @param status
     * @param message
     */
    public SqlResult(int status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * @return boolean
     */
    public boolean success() {
        return (this.status == 200);
    }

    /**
     * @param message
     * @return RunResult
     */
    public static SqlResult success(String message) {
        return new SqlResult(200, message);
    }

    /**
     * @param message
     * @return RunResult
     */
    public static SqlResult error(String message) {
        return new SqlResult(500, message);
    }

    /**
     * @return the status
     */
    public int getStatus() {
        return this.status;
    }
    
    /**
     * @param status the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }
    
    /**
     * @return the message
     */
    public String getMessage() {
        return this.message;
    }
    
    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    /**
     * @return the columns
     */
    public List<Column> getColumns() {
        return this.columns;
    }
    
    /**
     * @param columns the columns to set
     */
    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    /**
     * @return the records
     */
    public List<Record> getRecords() {
        return this.records;
    }

    /**
     * @param records the records to set
     */
    public void setRecords(List<Record> records) {
        this.records = records;
    }
}
