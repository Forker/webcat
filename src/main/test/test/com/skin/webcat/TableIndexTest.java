package test.com.skin.webcat;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.skin.webcat.database.ChangeIndex;

/**
 * @author weixian
 * @version 1.0
 */
public class TableIndexTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        String indexDefinition = "[{\"indexName\":{\"oldValue\":\"uk_flight\",\"newValue\":\"uk_flight\"},\"columnName\":{\"oldValue\":\"flight_no, flight_date, depart_code, arrive_code\",\"newValue\":\"flight_no, flight_date, depart_code, arrive_code\"},\"nonUnique\":{\"oldValue\":\"0\",\"newValue\":\"0\"},\"indexType\":{\"oldValue\":\"BTREE\",\"newValue\":\"BTREE\"}},{\"indexName\":{\"oldValue\":\"idx_pre_flight\",\"newValue\":\"idx_pre_flight\"},\"columnName\":{\"oldValue\":\"pre_flight_no, pre_flight_date, pre_depart_code, pre_arrive_code\",\"newValue\":\"pre_flight_no, pre_flight_date, pre_depart_code, pre_arrive_code\"},\"nonUnique\":{\"oldValue\":\"1\",\"newValue\":\"1\"},\"indexType\":{\"oldValue\":\"BTREE\",\"newValue\":\"BTREE\"}}]";
        List<ChangeIndex> indexChangeList = JSON.parseArray(indexDefinition, ChangeIndex.class);
        String createIndexSql = ChangeIndex.getCreateSql("test", indexChangeList);
        System.out.println(JSON.toJSONString(indexChangeList, false));
        System.out.println(createIndexSql);
    }
}
