/*
 * $RCSfile: TableTest.java,v $$
 * $Revision: 1.1 $
 * $Date: 2013-12-15 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package test.com.skin.webcat;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.skin.database.Jdbc;
import com.skin.j2ee.util.JsonUtil;
import com.skin.webcat.database.IndexInfo;
import com.skin.webcat.util.Webcat;

/**
 * <p>Title: TableTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class TableTest {

    /**
     * @param args
     */
    public static void main(String[] args) {
        showTableIndex();
    }

    /**
     * test1
     */
    public static void test1() { 
        ResultSet resultSet = null;
        Connection connection = null;
        String name = "localhost1";
        String database = "baike";
        List<IndexInfo> tableIndexList = new ArrayList<IndexInfo>();

        try {
            connection = Webcat.getConnection(name, database);
            DatabaseMetaData metaData = connection.getMetaData();
            resultSet = metaData.getIndexInfo(null, null, "bk_archive2", false, false);

            while(resultSet.next()) {
                IndexInfo indexInfo = new IndexInfo();
                indexInfo.setCatalog(resultSet.getString("TABLE_CAT"));
                indexInfo.setTableSchem(resultSet.getString("TABLE_SCHEM"));
                indexInfo.setTableName(resultSet.getString("TABLE_NAME"));
                indexInfo.setNonUnique((resultSet.getBoolean("NON_UNIQUE") ? 1 : 0));
                indexInfo.setIndexQualifier(resultSet.getString("INDEX_QUALIFIER"));
                indexInfo.setIndexName(resultSet.getString("INDEX_NAME"));
                indexInfo.setIndexType(resultSet.getString("TYPE"));
                indexInfo.setOrdinalPosition(resultSet.getInt("ORDINAL_POSITION"));
                indexInfo.setColumnName(resultSet.getString("COLUMN_NAME"));
                indexInfo.setAscOrDesc(resultSet.getString("ASC_OR_DESC"));
                indexInfo.setCardinality(resultSet.getInt("CARDINALITY"));
                indexInfo.setPages(resultSet.getInt("PAGES"));
                indexInfo.setFilterCondition(resultSet.getString("FILTER_CONDITION"));
                tableIndexList.add(indexInfo);
            }
            System.out.println(JSON.toJSONString(tableIndexList));
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        finally {
            Jdbc.close(resultSet);
            Jdbc.close(connection);
        }
    }

    /**
     * 
     */
    public static void test2() {
        ResultSet resultSet = null;
        Connection connection = null;
        String name = "localhost1";
        String database = "fmbak";
        List<IndexInfo> tableIndexList = new ArrayList<IndexInfo>();

        try {
            connection = Webcat.getConnection(name, database);
            DatabaseMetaData metaData = connection.getMetaData();
            resultSet = metaData.getImportedKeys(null, null, "bbs_forum_thread");

            while(resultSet.next()) {
                IndexInfo indexInfo = new IndexInfo();
                indexInfo.setCatalog(resultSet.getString("TABLE_CAT"));
                indexInfo.setTableSchem(resultSet.getString("TABLE_SCHEM"));
                indexInfo.setTableName(resultSet.getString("TABLE_NAME"));
                indexInfo.setNonUnique((resultSet.getBoolean("NON_UNIQUE") ? 1 : 0));
                indexInfo.setIndexQualifier(resultSet.getString("INDEX_QUALIFIER"));
                indexInfo.setIndexName(resultSet.getString("INDEX_NAME"));
                indexInfo.setIndexType(resultSet.getString("TYPE"));
                indexInfo.setOrdinalPosition(resultSet.getInt("ORDINAL_POSITION"));
                indexInfo.setColumnName(resultSet.getString("COLUMN_NAME"));
                indexInfo.setAscOrDesc(resultSet.getString("ASC_OR_DESC"));
                indexInfo.setCardinality(resultSet.getInt("CARDINALITY"));
                indexInfo.setPages(resultSet.getInt("PAGES"));
                indexInfo.setFilterCondition(resultSet.getString("FILTER_CONDITION"));
                tableIndexList.add(indexInfo);
            }
            System.out.println(JsonUtil.stringify(tableIndexList));
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        finally {
            Jdbc.close(resultSet);
            Jdbc.close(connection);
        }
    }

    /**
     * showTableIndex
     */
    public static void showTableIndex() {
        Statement statement = null;
        ResultSet resultSet = null;
        Connection connection = null;
        String name = "localhost1";
        String database = "baike";
        // List<IndexInfo> tableIndexList = new ArrayList<IndexInfo>();

        try {
            connection = Webcat.getConnection(name, database);
            statement = connection.createStatement();
            resultSet = statement.executeQuery("show index from bk_archive2");
            printResultSet(resultSet);
            // System.out.println(JSON.toJSONString(tableIndexList));
        }
        catch(SQLException e) {
            e.printStackTrace();
        }
        finally {
            Jdbc.close(resultSet);
            Jdbc.close(statement);
            Jdbc.close(connection);
        }
    }

    /**
     * @param resultSet
     * @throws SQLException
     */
    public static void printResultSet(ResultSet resultSet) throws SQLException {
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
        int columnCount = resultSetMetaData.getColumnCount();
        String[] columnNameList = new String[columnCount];
        
        for(int i = 1; i <= columnCount; i++) {
            columnNameList[i - 1] = resultSetMetaData.getColumnName(i);
            System.out.print(columnNameList[i - 1] + ", ");
        }
        System.out.println();

        while(resultSet.next()) {
            for(int i = 0; i < columnCount; i++) {
                System.out.println(columnNameList[i] + ": " + resultSet.getObject(i + 1));
            }
            System.out.println("====================================");
        }
    }
}
