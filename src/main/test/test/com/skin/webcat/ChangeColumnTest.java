/*
 * $RCSfile: ChangeColumnTest.java,v $
 * $Revision: 1.1 $
 * $Date: 2014-03-01 $
 *
 * Copyright (C) 2005 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package test.com.skin.webcat;

import java.io.File;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.skin.webcat.database.ChangeColumn;
import com.skin.webcat.database.ChangeContext;
import com.skin.webcat.util.IO;

/**
 * <p>Title: ChangeColumnTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class ChangeColumnTest {
    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            String tableDefinition = IO.read(new File("docs/testdata/columnChangeList.txt"), "utf-8");
            List<ChangeColumn> changeList = JSON.parseArray(tableDefinition, ChangeColumn.class);
            ChangeContext changeContext = new ChangeContext(null, changeList, null);
            String sql = changeContext.getAlterSql();

            if(sql == null || sql.length() < 1) {
                System.out.println("no change");
            }
            else {
                System.out.println(sql);
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
}
