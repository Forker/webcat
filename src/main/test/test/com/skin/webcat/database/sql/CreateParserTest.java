/*
 * $RCSfile: CreateParserTest.java,v $$
 * $Revision: 1.1 $
 * $Date: 2014-03-25 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package test.com.skin.webcat.database.sql;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.skin.webcat.database.Table;
import com.skin.webcat.database.dialect.MySQLDialect;
import com.skin.webcat.database.sql.parser.CreateParser;
import com.skin.webcat.database.sql.parser.SqlParser;
import com.skin.webcat.util.IO;
import com.skin.webcat.util.StringStream;

/**
 * <p>Title: CreateParserTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * @author xuesong.net
 * @version 1.0
 */
public class CreateParserTest {
    /**
     * @param args
     */
    public static void main(String[] args) {
        test1(new File("docs\\test.sql"));
    }

    /**
     * @param file
     */
    public static void test1(File file) {
        try {
            String source = IO.read(file, "utf-8");
            CreateParser parser = new CreateParser(new MySQLDialect());
            List<Table> tableList = parser.parse(source);

            for(Table table : tableList) {
                System.out.println("============================");
                System.out.println(JSON.toJSONString(table, true));
            }
            System.out.println("table count: " + tableList.size());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param source
     */
    public static void getAttrTest(String source) {
        try {
            StringStream stream = new StringStream(source);
            Map<String, String> attributes = SqlParser.getAttributes(stream);
            System.out.println(attributes);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     */
    public static void getWordTest() {
        try {
            StringStream stream = new StringStream("  '0.00'   \r\n;");
            String word = SqlParser.getWord(stream);
            System.out.println(word);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
