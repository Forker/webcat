/*
 * $RCSfile: JSONParserTest.java,v $$
 * $Revision: 1.1 $
 * $Date: 2014-03-25 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package test.com.skin.webcat.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import com.alibaba.fastjson.JSON;
import com.skin.ayada.util.MemMonitor;
import com.skin.util.IO;
import com.skin.webcat.util.JSONParser;

/**
 * <p>Title: JSONParserTest</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2014</p>
 * @author xuesong.net
 * @version 1.0
 */
public class JSONParserTest2 {
    /**
     * @param args
     */
    public static void main(String[] args) {
        String source1 = "{/** */ za  /* dsfasfdasfsfdsa\r\ndasfaf\r\n */  : \r\n{c1: \r\n1, xd: \r\n2, ae: \"3\\n567\\n33\", cf: [\r\n1, 2, 3, 4]}, xb: \r\n2, ec: \r\n   \r\n  true, ad: -456, ya: undefined}";
        String source2 = getSource(new File("white.txt"), "utf-8");
        source2 = "{x:1,c:2,a:3,b:4,d:5,e:6,f:7}";
        source2 = source1;

        // source = "{\"a\":\"{\\\"b\\\":\\\"3\\\"}\"}";
        // System.out.println(source);
        System.out.println(new JSONParser().parse(source1));

        if(getBoolean(true)) {
            // return;
        }

        // test1(source1, 100);
        test2(source1, 100);
        System.gc();

        PrintWriter out = new PrintWriter(System.out);
        MemMonitor monitor = new MemMonitor();
        monitor.test(out, true, true);

        int count = 10 * 10000;
        System.out.println("----------------- test1 start -----------------");
        test1(source2, count);
        // monitor.test(out, true, true);
        System.gc();

        System.out.println("----------------- test1 start -----------------");
        test2(source2, count);
    }

    /**
     * @param source
     * @param count
     */
    public static void test1(String source, int count) {
        JSONParser parser = new JSONParser();
        long t1 = System.currentTimeMillis();

        for(int i = 0; i < count; i++) {
            parser.parse(source);
        }

        long t2 = System.currentTimeMillis();
        System.out.println("myparser[" + count + "]: " + (t2 - t1));
    }

    /**
     * @param source
     * @param count
     */
    public static void test2(String source, int count) {
        long t1 = System.currentTimeMillis();

        for(int i = 0; i < count; i++) {
            JSON.parse(source);
        }

        long t2 = System.currentTimeMillis();
        System.out.println("fastjson[" + count + "]: " + (t2 - t1));
    }

    /**
     * @param file
     * @param charset
     * @return String
     */
    public static String getSource(File file, String charset) {
        try {
            return IO.read(file, "utf-8");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param b
     * @return boolean
     */
    public static boolean getBoolean(boolean b) {
        return b;
    }
}
