/*
 * $RCSfile: BeanHelper.java,v $$
 * $Revision: 1.1 $
 * $Date: 2016-11-5 $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package test.com.skin.webcat;

import java.lang.reflect.Method;
import java.util.Date;

import com.skin.j2ee.util.DaoUtil;
import com.skin.webcat.database.mysql.TableIndex;

/**
 * <p>Title: BeanHelper</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author xuesong.net
 * @version 1.0
 */
public class BeanHelper {
    /**
     * @param args
     */
    public static void main(String[] args) {
        generate(TableIndex.class);
    }

    /**
     * @param type
     */
    public static void generate(Class<?> type) {
        Method[] methods = type.getMethods();
        String beanName = getVariable(type);

        try {
            for(int i = 0; i < methods.length; i++) {
                String name = methods[i].getName();
                Class<?>[] parameterTypes = methods[i].getParameterTypes();

                if(name.startsWith("set") && name.length() > 3 && parameterTypes.length == 1) {
                    Class<?> parameterType = parameterTypes[0];
                    String columnName = DaoUtil.getColumnName(name.substring(3)).toUpperCase();

                    if(parameterType == int.class) {
                        System.out.println(beanName + "." + name + "(resultSet.getInt(\"" + columnName + "\"));");
                    }
                    else if(parameterType == float.class) {
                        System.out.println(beanName + "." + name + "(resultSet.getFloat(\"" + columnName + "\"));");
                    }
                    else if(parameterType == double.class) {
                        System.out.println(beanName + "." + name + "(resultSet.getDouble(\"" + columnName + "\"));");
                    }
                    else if(parameterType == long.class) {
                        System.out.println(beanName + "." + name + "(resultSet.getLong(\"" + columnName + "\"));");
                    }
                    else if(parameterType == String.class) {
                        System.out.println(beanName + "." + name + "(resultSet.getString(\"" + columnName + "\"));");
                    }
                    else if(parameterType == Date.class) {
                        System.out.println(beanName + "." + name + "(resultSet.getTimestamp(\"" + columnName + "\"));");
                    }
                    else {
                        System.out.println(beanName + "." + name + "(resultSet.getString(\"" + columnName + "\"));");
                    }
                }
            }
        }
        catch(Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * @param type
     * @return String
     */
    private static String getVariable(Class<?> type) {
        String simpleName = type.getSimpleName();
        return Character.toLowerCase(simpleName.charAt(0)) + simpleName.substring(1);
    }
}
