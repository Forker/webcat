<t:include file="/include/common.jsp"/>
/*
 * $RCSfile: ${daoImplClassName}.java,v $$
 * $Revision: 1.1 $
 * $Date: ${timestamp} $
 *
 * Copyright (C) 2008 Skin, Inc. All rights reserved.
 *
 * This software is the proprietary information of Skin, Inc.
 * Use is subject to license terms.
 */
package ${daoImplPackageName};

import javax.annotation.Resource;
import org.springframework.stereotype.Component;

import ${daoItfcPackageName}.${daoItfcClassName};
import ${modelPackageName}.${modelClassName};

/**
 * <p>Title: ${daoImplClassName}</p>
 * <p>Description: ${variableContext.getString("Description")}</p>
 * <p>Copyright: Copyright (c) 2006</p>
 * @author ${author}
 * @version 1.0
 */
@Component
public class ${daoImplClassName} implements ${daoItfcClassName} {
    /**
     * @param sqlMapClientTemplate
     */
    @Resource
    protected void setSqlTemplate(@Resource("daoTemplate") SqlMapClientTemplate sqlMapClientTemplate) {
        super.setSqlTemplate(sqlMapClientTemplate);
    }
}
