<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title>SQL [${fileName}]</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" type="image/x-icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/widget/css/widget.css"/>
<script type="text/javascript" src="${contextPath}/resource/widget/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/widget/widget.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/sql-table-list.js"></script>
</head>
<c:set var="targetUrl" value="/webcat/sql/edit.html?fileName=${URLUtil.encode(fileName)}"/>
<body style="overflow: hidden;">
<div id="table-list-panel" class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">SQL [${fileName}]</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="panel">
                <div class="menu-bar">
                    <a id="tools-btn" class="button">常用工具</a>
                    <a id="batch-generate-btn" class="button">批量生成</a>
                    <a id="finder-btn" class="button">查看文件</a>
                    <a id="export-sql-btn" class="button">导出脚本</a>
                </div>
            </div>
            <div class="list-panel" class="scroll-d">
                <table class="table">
                    <tr class="thead">
                        <td class="w80"><input type="checkbox" checked="true" title="全 选" onclick="DomUtil.check('tableName', this.checked)"/></td>
                        <td class="w400">Table & View</td>
                        <td>操 作</td>
                    </tr>
                    <c:forEach items="${tableList}" var="table" varStatus="status">
                    <tr>
                        <td class="w60 center"><input type="checkbox" name="tableName" value="${table.tableName}" tableType="${table.tableType}" checked="true"/></td>
                        <td>
                            <img src="${contextPath}/resource/webcat/images/table.gif"/>
                            <a href="javascript:void(0)" onclick="DomUtil.show('table-panel-${table.tableName}');" title="表结构">${table.tableName}</a -->
                        </td>
                        <td>
                            <a href="/webcat/sql/insert.html?fileName=${URLUtil.encode(fileName)}&tableName=${URLUtil.encode(table.tableName)}&templateConfig=${URLUtil.encode(templateConfig)}">插入编辑</a>
                            <a href="/webcat/sql/generate.html?fileName=${URLUtil.encode(fileName)}&tableName=${URLUtil.encode(table.tableName)}&templateConfig=${URLUtil.encode(templateConfig)}">生成代码</a>
                        </td>
                    </tr>
                    </c:forEach>
                </table>
            </div>
        </div>
    </div>
</div>
<c:forEach items="${tableList}" var="table" varStatus="status">
<div id="table-panel-${table.tableName}" class="dialog hide" style="position: absolute; top: 20px; left: 20px;">
    <div class="panel">
        <div class="panel-title" dragable="true">
            <h4>${table.tableName}</h4>
            <span class="close"></span>
        </div>
        <div class="panel-content" style="width: 840px; height: 360px; overflow: auto; cursor: default;">
            <table class="table">
                <tr class="thead">
                    <td class="cc w60 bb">index</td>
                    <td class="w200 bb">columnName</td>
                    <td class="w200 bb">columnType</td>
                    <td class="bb">remarks</td>
                </tr>
                <c:forEach items="${table.getColumns()}" var="column" varStatus="status">
                <tr title="${column.typeName}: ${column.remarks}">
                    <td class="cc bb">${status.index + 1}</td>
                    <td class="bb">${column.columnName}</td>
                    <td>${column.typeName}(${column.precision})</td>
                    <td>${column.remarks}</td>
                </tr>
                </c:forEach>
            </table>
            <div style="height: 30px;"></div>
        </div>
    </div>
</div>
</c:forEach>
<div class="hide">
    <form name="exportForm"></form>
</div>
<div id="pageContext" contextPath="${contextPath}" fileName="${fileName}" connectionName="${connectionName}"></div>
</body>
</html>