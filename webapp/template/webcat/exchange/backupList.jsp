<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>数据备份</title>
<meta name="keywords" content="${siteConfig.title}"/>
<meta name="description" content="${siteConfig.title}"/>
<meta name="robots" content="all"/>
<meta name="googlebot" content="all"/>
<meta name="baiduspider" content="all"/>
<meta name="copyright" content="${HtmlUtil.remove(siteConfig.copyright)}"/>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/style/webcat.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/tabpanel/style/tab-panel.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/tabpanel/tab-panel.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
</head>
<body>
<div class="menu-panel"><h4>历史备份</h4></div>
<div class="menu-bar">
    <a class="button" href="javascript:void(0)" onclick="window.history.back();">返 回</a>
    <a class="button" href="javascript:void(0)" onclick="window.location.reload();">刷 新</a>
</div>
<table class="list">
    <tr class="head">
        <td class="w30">&nbsp;</td>
        <td class="w300">备份文件</td>
        <td class="w200">文件大小</td>
        <td>操作</td>
    </tr>
    <c:forEach items="${fileList}" var="file" varStatus="status">
    <tr>
        <td>${status.index + 1}</td>
        <td>${file.name}</td>
        <td>${file.bytes}</td>
        <td>
            <a href="/webcat/backup/detail.html?zip=${file.name}">详 情</a>
            <a href="/webcat/backup/download.html?zip=${file.name}">下 载</a>
        </td>
    </tr>
    </c:forEach>
</table>
<%@include file="/include/common/footer.jsp"%>
</body>
</html>