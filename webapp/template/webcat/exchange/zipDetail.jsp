<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>数据备份</title>
<meta name="keywords" content="${siteConfig.title}"/>
<meta name="description" content="${siteConfig.title}"/>
<meta name="robots" content="all"/>
<meta name="googlebot" content="all"/>
<meta name="baiduspider" content="all"/>
<meta name="copyright" content="${HtmlUtil.remove(siteConfig.copyright)}"/>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7"/>
<link rel="shortcut icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/style/webcat.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/tabpanel/style/tab-panel.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/tabpanel/tab-panel.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
</head>
<body>
<div class="menu-panel"><h4>历史备份</h4></div>
<div class="menu-bar">
    <a class="button" href="javascript:void(0)" onclick="window.history.back();">返 回</a>
    <a class="button" href="javascript:void(0)" onclick="window.location.reload();">刷 新</a>
</div>
<div style="line-height: 30px;">
    备份文件：<a href="/webcat/backup/download.html?zip=${zip}">${zip}</a>
</div>
<table class="list">
    <tr class="head">
        <td class="w30">&nbsp;</td>
        <td class="w300">名 称</td>
        <td class="w100">大 小</td>
        <td class="w100">压缩后大小</td>
        <td class="w150">修改时间</td>
        <td>操作</td>
    </tr>
    <c:forEach items="${entryList}" var="zipEntry" varStatus="status">
    <tr>
        <td>${status.index + 1}</td>
        <td>${zipEntry.name}</td>
        <td style="padding-right: 8px; text-align: right;">${zipEntry.size}</td>
        <td style="padding-right: 8px; text-align: right;">${zipEntry.compressedSize}</td>
        <td><fmt:formatDate value="${zipEntry.time}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
        <td><a class="btn" href="/webcat/backup/download.html?zip=${zip}&name=${zipEntry.name}">下载</a></td>
    </tr>
    </c:forEach>
</table>
<%@include file="/include/common/footer.jsp"%>
</body>
</html>