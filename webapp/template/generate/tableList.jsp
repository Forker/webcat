<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Pragma" content="no-cache"/>
<meta http-equiv="Cache-Control" content="no-cache"/>
<meta http-equiv="Expires" content="0"/>
<link rel="shortcut icon" type="image/x-icon" href="${contextPath}/favicon.ico"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/webcat/style/webcat.css"/>
<link rel="stylesheet" type="text/css" href="${contextPath}/resource/tabpanel/style/tab-panel.css"/>
<script type="text/javascript" src="${contextPath}/resource/webcat/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/ajax.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/util.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/json2.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/drag.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/resize.js"></script>
<script type="text/javascript" src="${contextPath}/resource/webcat/webcat.js"></script>
<script type="text/javascript" src="${contextPath}/resource/tabpanel/tab-panel.js"></script>
<script type="text/javascript" src="${contextPath}/resource/generate/table-list.js"></script>
</head>
<body style="overflow: hidden;" fileName="${fileName}" connectionName="${connectionName}" templateConfig="${templateConfig}">
<c:if test="${util.isEmpty(fileName)}">
    <c:set var="targetUrl" value="/table/edit.html?connectionName=${connectionName}"/>
</c:if>
<c:if test="${util.notEmpty(fileName)}">
    <c:set var="targetUrl" value="/sql/edit.html?fileName=${URLUtil.encode(fileName)}"/>
</c:if>
<div class="tab-component">
    <div class="tab-label-wrap">
        <ul>
            <li class="tab-label tab-active"><span class="label">批量生成</span></li>
        </ul>
    </div>
    <div class="tab-panel-wrap">
        <div class="tab-panel">
            <div class="panel">
                <div class="menu-bar">
                    <a id="tools-btn" class="button" href="javascript:void(0)">常用工具</a>
                    <a id="batch-btn" class="button" href="javascript:void(0)">批量生成</a>
                    <a id="finder-btn" class="button" href="javascript:void(0)" target="_blank">查看文件</a>
                    <a id="export-script-btn" class="button" href="javascript:void(0)">导出脚本</a>
                    <c:if test="${util.isEmpty(fileName)}">
                    <a id="export-data-btn" class="button" href="javascript:void(0)">导出数据</a>
                    </c:if>
                </div>
                <div class="scroll-d">
                    <table class="table">
                        <tr class="thead">
                            <td class="w40"><input type="checkbox" checked="true" title="全 选" onclick="Util.check('tableName', this.checked)"/></td>
                            <td class="w200">表名</td>
                            <td class="w300">描述</td>
                            <td>操作</td>
                        </tr>
                        <c:forEach items="${tableList}" var="table" varStatus="status">
                        <c:set var="tableId" value="drag_target_${table.tableName}"/>
                        <tr>
                            <td class="w60 center"><input type="checkbox" name="tableName" value="${table.tableName}" tableType="${table.tableType}" checked="true"/></td>
                            <td>
                                <img src="/resource/webcat/images/table.gif"/>
                                <!-- a href="${targetUrl}&templateConfig=${URLUtil.encode(templateConfig)}&tableName=${URLUtil.encode(table.tableName)}" title="${connectionConfig.url}">${table.tableName}</a -->
                                <a href="javascript:void(0)" onclick="Util.show('${tableId}');document.getElementById('${tableId}').style.zIndex = (SimpleDrag.zIndex++);" title="表结构">${table.tableName}</a -->
                            </td>
                            <td>${table.remarks}</td>
                            <td>
                                <a href="/sql/insert.html?fileName=${URLUtil.encode(fileName)}&tableName=${URLUtil.encode(table.tableName)}&templateConfig=${URLUtil.encode(templateConfig)}">插入编辑</a>
                                <c:if test="${util.isEmpty(fileName)}">
                                <a href="/table/edit.html?connectionName=${URLUtil.encode(connectionName)}&tableName=${URLUtil.encode(table.tableName)}&templateConfig=${URLUtil.encode(templateConfig)}">生成代码</a>
                                </c:if>
                                <c:if test="${util.notEmpty(fileName)}">
                                <a href="/sql/edit.html?fileName=${URLUtil.encode(fileName)}&tableName=${URLUtil.encode(table.tableName)}&templateConfig=${URLUtil.encode(templateConfig)}">生成代码</a>
                                </c:if>
                            </td>
                        </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<c:forEach items="${tableList}" var="table" varStatus="status">
    <c:set var="tableId" value="drag_target_${table.tableName}"/>
    <div id="${tableId}" class="panel hide" style="position: absolute; top: 20px; left: 40px;">
        <div class="panel-title">
            <h4 id="drag_source_${table.tableName}">
                <span class="icon-table"></span>${table.tableName}
                <span class="button close" onclick="Util.hide('${tableId}')"></span>
            </h4>
        </div>
        <div class="panel-content">
            <div class="form-panel" style="width: 840px; height: 480px; overflow: auto; cursor: default;">
                <table class="table" style="width: 800px;">
                    <tr class="thead">
                        <td class="cc w60 bb">index</td>
                        <td class="w200 bb">columnName</td>
                        <td class="w200 bb">columnType</td>
                        <td class="bb">remarks</td>
                    </tr>
                    <c:forEach items="${table.listColumns()}" var="column" varStatus="status">
                    <tr title="${column.typeName}: ${column.remarks}">
                        <td class="cc bb">${status.index + 1}</td>
                        <td class="bb">${column.columnName}</td>
                        <td>${column.typeName}(${column.precision})</td>
                        <td>${column.remarks}</td>
                    </tr>
                    </c:forEach>
                </table>
                <div style="height: 30px;"></div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    <!--
    (function(){
        SimpleDrag.register("drag_source_${table.tableName}", "drag_target_${table.tableName}");
    })();
    //-->
    </script>
</c:forEach>
<div class="hide">
    <form name="exportForm"></form>
</div>
</body>
</html>